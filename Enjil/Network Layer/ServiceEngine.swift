
import UIKit
import MobileCoreServices

public enum dispatchMethod:String{
    case get = "GET",
    post = "POST",
    put = "PUT",
    delete = "DELETE"
}

typealias ULong = UInt64
public typealias APICompletionHandler = (_ success:Bool,_ result:Any?) -> Void

extension Data {
    private static let mimeTypeSignatures: [UInt8 : String] = [
        0xFF : "image/jpeg",
        0x89 : "image/png",
        0x47 : "image/gif",
        0x49 : "image/tiff",
        0x4D : "image/tiff",
        0x25 : "application/pdf",
        0xD0 : "application/vnd",
        0x46 : "text/plain",
    ]
    
    var mimeType: String {
        var c: UInt8 = 0
        copyBytes(to: &c, count: 1)
        return Data.mimeTypeSignatures[c] ?? "application/octet-stream"
    }
    
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}

enum StatusCode:Int{
    case success = 200
    case error = 400
    case blocked1 = 401
    case blocked2 = 403
    case blocked3 = 405
}

public enum LogLevel{
    case none
    case verbose
}

public class ServiceEngine: NSObject {
    
    public var uploadTasks = [GenericUploadTask]()
    private var session:URLSession!
    public static let shared = ServiceEngine()
    
    static var latestRequest:URLRequest!
    static var latestHandler:APICompletionHandler!
    
    static fileprivate func printResponseHeaders(_ response:HTTPURLResponse){
        
        #if DEBUG
        print("\(response.allHeaderFields)")
        #endif
    }
    
    override init(){
        super.init()
        let configuration = URLSessionConfiguration.default
        session = URLSession(configuration: configuration,
                             delegate: self, delegateQueue: OperationQueue.main)
        
    }
    
    static fileprivate func deleteCookies(){
        let cookieJar = HTTPCookieStorage.shared
        
        for cookie in cookieJar.cookies! {
            print(cookie.name+"="+cookie.value)
            cookieJar.deleteCookie(cookie)
        }
    }
    
    static private func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    static private func createBody(with parameters: [String: Any]?, filePathKey: [String]?,filename: [String]?,dataArr:[Data]?, boundary: String) -> Data {
        var body = Data()
        
        if let params = parameters{
            for (key, value) in params {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value)\r\n")
            }
        }
        
        if let arr = dataArr,let fName = filename{
            for (index,data) in arr.enumerated(){
                print(data.mimeType)
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\(fName[index]); filename=\"\(fName[index]).jpg\"\r\n")
                body.append("Content-Type: \(data.mimeType)\r\n\r\n")
                body.append(data)
                body.append("\r\n")
            }
        }
        
        body.append("--\(boundary)--\r\n")
        return body
    }
    
    public static func constructMultiPartRequest(url:URL,bodyparams:[String:Any])->URLRequest{

          var request = URLRequest(url: url)

          let boundary = generateBoundaryString()

          request.httpMethod = "POST"

          request.httpBody = createBody(with: bodyparams, filePathKey: nil,filename:nil,dataArr:nil, boundary: boundary)

          request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

          return request

       }


    
    public static func dispatchRequestMultiPart(path:String,header:[String:Any]?=nil,filePathKey:[String]=["file"],bodyparams:[String:Any]?,fileName:[String]?,imageData:[Data]?,methodPost: dispatchMethod = .post,completionHandler:APICompletionHandler?){
        let newPath = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url:URL = URL(string:newPath)!
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = methodPost.rawValue
        request.timeoutInterval = 120
        
        if let header = header{
            for key in header.keys{
                if let value = header[key] as? String{
                    request.setValue(value, forHTTPHeaderField: key)
                }
            }
        }
        
        let boundary = generateBoundaryString()
        request.httpBody = createBody(with: bodyparams, filePathKey: filePathKey,filename:fileName,dataArr:imageData, boundary: boundary)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            if let httpResponse = response as? HTTPURLResponse {
                self.printResponseHeaders(httpResponse)
                print("Response code: \(httpResponse.statusCode)")
            }
            
            if let error = error{
                print(error.localizedDescription)
                if let handler = completionHandler{
                    handler(false,error.localizedDescription)
                }
                return
            }
            
            
            guard let data = data, let _:URLResponse = response else {
                print("Data empty")
                if let handler = completionHandler{
                    handler(false,response)
                }
                return
            }
            
            let dataString =  String(data: data, encoding: String.Encoding.utf8)
            print(dataString!)
            
            if let handler = completionHandler{
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? Any{
                        
                        if let httpResponse = response as? HTTPURLResponse {
                            self.printResponseHeaders(httpResponse)
                            print("Response code: \(httpResponse.statusCode)")
                            
                            if let code = StatusCode(rawValue: httpResponse.statusCode){
                                switch code{
                                case .success:
                                    handler(true,data)
                                    break
                                case .blocked1:
                                    break
                                default:
                                    handler(false,json)
                                    break
                                }
                            }else{
                                handler(false,json)
                            }
                        }
                        
                    } else{
                        handler(false,"Json could not be parsed")
                    }
                } catch {
                    print(error.localizedDescription)
                    handler(false,error)
                }
                
            }
            
        }
        
        task.resume()
    }
    
    public func uploadFile(path:String,header:[String:Any]?=nil,bodyparams:[String:Any],fileName:[String]?,imageData:[Data]? )->UploadTask{
        
        let newPath = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url:URL = URL(string:newPath)!
        
        var request = URLRequest(url: url)
        request.httpMethod = dispatchMethod.post.rawValue
        request.timeoutInterval = 120
        
        if let header = header{
            for key in header.keys{
                if let value = header[key] as? String{
                    request.setValue(value, forHTTPHeaderField: key)
                }
            }
        }
        
        let boundary = ServiceEngine.generateBoundaryString()
        request.httpBody = ServiceEngine.createBody(with: bodyparams, filePathKey: ["file"],filename:fileName,dataArr:imageData, boundary: boundary)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let task = session.dataTask(with: request)
        let uploadTask = GenericUploadTask(task: task)
        uploadTasks.append(uploadTask)
        return uploadTask
    }
    
    static func constructRequest(path:String,header:[String:Any]?=nil,uriparams:[String:Any]?,bodyparams:[String:Any]?,methodPost: dispatchMethod = .get)->URLRequest{
        var paramString = ""
        if let params = uriparams{
            paramString = "?"
            for key in params.keys{
                let value = params[key]!
                
                if value is String{
                    let str = value as! String
                    if !str.isEmpty{
                        paramString += "\(key)=\(value)&"
                    }
                }else{
                    paramString += "\(key)=\(value)&"
                }
            }
            
            paramString.removeLast()
        }
        
        if let bodyparams = bodyparams{
            print(bodyparams)
        }
        var newPath = "\(path)\(paramString)"
        print("Path = \(newPath)")
        newPath = newPath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url:URL = URL(string:newPath)!
        
        var request = URLRequest(url: url)
        request.httpMethod = methodPost.rawValue
        request.timeoutInterval = 120
        
        if let header = header{
            for key in header.keys{
                if let value = header[key] as? String{
                    request.setValue(value, forHTTPHeaderField: key)
                }
            }
        }
        
        if let params = bodyparams{
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                let theJSONText = String(data: request.httpBody!,
                                         encoding: .ascii)
                print(theJSONText!);
            } catch let error {
                print(error.localizedDescription)
            }
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
        }
        return request
    }
    
    public static func dispatchRequest(path:String,header:[String:Any]?=nil,uriparams:[String:Any]?,bodyparams:[String:Any]?,methodPost: dispatchMethod = .get,ignoreResponse:Bool = false,logLevel:LogLevel = LogLevel.none,completionHandler:APICompletionHandler?){
        
        let request = constructRequest(path: path, header: header, uriparams: uriparams, bodyparams: bodyparams, methodPost: methodPost)
        latestRequest = request
        latestHandler = completionHandler
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            if ignoreResponse{
                if let handler = completionHandler{
                    handler(true,nil)
                }
                return
            }
            
            if let error = error{
                print(error.localizedDescription)
                if let handler = completionHandler{
                    handler(false,error.localizedDescription)
                }
                return
            }
            
            
            guard let data = data, let _:URLResponse = response else {
                print("Data empty")
                if let handler = completionHandler{
                    handler(false,response)
                }
                return
            }
            
            if let dataString =  String(data: data, encoding: String.Encoding.utf8){
                if logLevel == .verbose{
                    print(dataString)
                }
            }
            
            if let handler = completionHandler{
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? Any{
                        
                        if let httpResponse = response as? HTTPURLResponse {
                            self.printResponseHeaders(httpResponse)
                            print("Response code: \(httpResponse.statusCode)")
                            
                            if let code = StatusCode(rawValue: httpResponse.statusCode){
                                switch code{
                                case .success:
                                    handler(true,data)
                                    break
                                default:
                                    handler(false,json)
                                    break
                                }
                            }else{
                                handler(false,json)
                            }
                        }
                        
                    } else{
                        handler(false,"Json could not be parsed")
                    }
                } catch {
                    print(error.localizedDescription)
                    handler(false,error)
                }
                
            }
            
        }
        
        task.resume()
    }
    
    public static func dispatchRequest(request:URLRequest,completionHandler:APICompletionHandler?){
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            if let error = error{
                print(error.localizedDescription)
                if let handler = completionHandler{
                    handler(false,error.localizedDescription)
                }
                return
            }
            
            
            guard let data = data, let _:URLResponse = response else {
                print("Data empty")
                if let handler = completionHandler{
                    handler(false,response)
                }
                return
            }
            
            if let dataString =  String(data: data, encoding: String.Encoding.utf8){
                print(dataString)
            }
            
            if let handler = completionHandler{
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? Any{
                        
                        if let httpResponse = response as? HTTPURLResponse {
                            self.printResponseHeaders(httpResponse)
                            print("Response code: \(httpResponse.statusCode)")
                            
                            if let code = StatusCode(rawValue: httpResponse.statusCode){
                                switch code{
                                case .success:
                                    handler(true,data)
                                    break
                                case .blocked1:
                                    break
                                default:
                                    handler(false,json)
                                    break
                                }
                            }else{
                                handler(false,json)
                            }
                        }
                        
                    } else{
                        handler(false,"Json could not be parsed")
                    }
                } catch {
                    print(error.localizedDescription)
                    handler(false,error)
                }
                
            }
            
        }
        
        task.resume()
    }
    
    public static func dispatchImageRequest(path:String,header:[String:Any]?=nil,uriparams:[String:Any]?,bodyparams:[String:Any]?,methodPost: dispatchMethod = .get,completionHandler:APICompletionHandler?){
        
        var paramString = ""
        if let params = uriparams{
            paramString = "?"
            for key in params.keys{
                let value = params[key]!
                
                if value is String{
                    let str = value as! String
                    if !str.isEmpty{
                        paramString += "\(key)=\(value)&"
                    }
                }else{
                    paramString += "\(key)=\(value)&"
                }
            }
            
            paramString.removeLast()
        }
        
        if let bodyparams = bodyparams{
            print(bodyparams)
        }
        var newPath = "\(path)\(paramString)"
        print("Path = \(newPath)")
        newPath = newPath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let url:URL = URL(string:newPath)!
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = methodPost.rawValue
        request.timeoutInterval = 120
        
        if let header = header{
            for key in header.keys{
                if let value = header[key] as? String{
                    request.setValue(value, forHTTPHeaderField: key)
                }
            }
        }
        
        if let params = bodyparams{
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                let theJSONText = String(data: request.httpBody!,
                                         encoding: .ascii)
                print(theJSONText!);
            } catch let error {
                print(error.localizedDescription)
            }
            
            request.addValue("application/json", forHTTPHeaderField: "Accept")
        }
        
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            if let error = error{
                print(error.localizedDescription)
                if let handler = completionHandler{
                    handler(false,error.localizedDescription)
                }
                return
            }
            
            
            guard let data = data, let _:URLResponse = response else {
                print("Data empty")
                if let handler = completionHandler{
                    handler(false,response)
                }
                return
            }
            
            
            if let handler = completionHandler{
           
                if let httpResponse = response as? HTTPURLResponse {
                    self.printResponseHeaders(httpResponse)
                    print("Response code: \(httpResponse.statusCode)")
                    
                    if let code = StatusCode(rawValue: httpResponse.statusCode){
                        if code != .success{
                            handler(false,nil)
                        }else{
                            handler(true,data)
                        }
                    }else{
                        handler(false,nil)
                    }
                }
            }
            
        }
        
        task.resume()
    }
}

public class GenericUploadTask {
    
    public var completionHandler: APICompletionHandler?
    public var progressHandler: ((Double) -> Void)?
    
    private(set) var task: URLSessionDataTask
    var expectedContentLength: Int64 = 0
    var buffer = Data()
    
    init(task: URLSessionDataTask) {
        self.task = task
    }
    
    deinit {
        print("Deinit: \(task.originalRequest?.url?.absoluteString ?? "")")
    }
    
}

public protocol UploadTask {
    
    //var APICompletionHandler = (_ success:Bool,_ result:Any?) -> Void
    var completionHandler: APICompletionHandler? { get set }
    var progressHandler: ((Double) -> Void)? { get set }
    
    func resume()
    func suspend()
    func cancel()
}


extension GenericUploadTask: UploadTask {
    
    public func resume() {
        task.resume()
    }
    
    public func suspend() {
        task.suspend()
    }
    
    public func cancel() {
        task.cancel()
    }
}



extension ServiceEngine: URLSessionDataDelegate {
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
    }
    public func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        
        guard let task = uploadTasks.first(where: { $0.task == task }) else {
            return
        }
        
        let percentageDownloaded = Double(totalBytesSent) / Double(totalBytesExpectedToSend)
        DispatchQueue.main.async {
            task.progressHandler?(percentageDownloaded)
        }
        
    }
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        
        guard let task = uploadTasks.first(where: { $0.task == dataTask }) else {
            completionHandler(.cancel)
            return
        }
        task.expectedContentLength = response.expectedContentLength
        completionHandler(.allow)
    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        guard let task = uploadTasks.first(where: { $0.task == dataTask }) else {
            return
        }
        task.buffer.append(data)
        let percentageDownloaded = Double(task.buffer.count) / Double(task.expectedContentLength)
        DispatchQueue.main.async {
            task.progressHandler?(percentageDownloaded)
        }
    }
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard let index = uploadTasks.index(where: { $0.task == task }) else {
            return
        }
        let task = uploadTasks.remove(at: index)
        DispatchQueue.main.async {
            if let _ = error {
                task.completionHandler?(false,nil)
            } else {
                task.completionHandler?(true,task.buffer)
            }
        }
    }
    
}

