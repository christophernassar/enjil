
//
//  ServiceInterface.swift
//  Rawa
//
//  Created by Imac on 5/6/19.
//  Copyright © 2019 Imac. All rights reserved.
//

import UIKit
import KeychainAccess

class ServiceInterface {
    static let hostURL = Helper.getHostURL()
    static let resourceURL = Helper.getResourceURL()
}

enum HMACAlgorithm {
    case MD5, SHA1, SHA224, SHA256, SHA384, SHA512
    
    func toCCHmacAlgorithm() -> CCHmacAlgorithm {
        var result: Int = 0
        switch self {
        case .MD5:
            result = kCCHmacAlgMD5
        case .SHA1:
            result = kCCHmacAlgSHA1
        case .SHA224:
            result = kCCHmacAlgSHA224
        case .SHA256:
            result = kCCHmacAlgSHA256
        case .SHA384:
            result = kCCHmacAlgSHA384
        case .SHA512:
            result = kCCHmacAlgSHA512
        }
        return CCHmacAlgorithm(result)
    }
    
    func digestLength() -> Int {
        var result: CInt = 0
        switch self {
        case .MD5:
            result = CC_MD5_DIGEST_LENGTH
        case .SHA1:
            result = CC_SHA1_DIGEST_LENGTH
        case .SHA224:
            result = CC_SHA224_DIGEST_LENGTH
        case .SHA256:
            result = CC_SHA256_DIGEST_LENGTH
        case .SHA384:
            result = CC_SHA384_DIGEST_LENGTH
        case .SHA512:
            result = CC_SHA512_DIGEST_LENGTH
        }
        return Int(result)
    }
}

extension String {
    func hmac(algorithm: HMACAlgorithm, key: String) -> String {
        let cKey = key.cString(using: String.Encoding.utf8)
        let cData = self.cString(using: String.Encoding.utf8)
        var result = [CUnsignedChar](repeating: 0, count: Int(algorithm.digestLength()))
        CCHmac(algorithm.toCCHmacAlgorithm(), cKey!, Int(strlen(cKey!)), cData!, Int(strlen(cData!)), &result)
        let hmacData:NSData = NSData(bytes: result, length: (Int(algorithm.digestLength())))
        let hmacBase64 = hmacData.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength76Characters)
        return String(hmacBase64)
    }
}

extension ServiceInterface{
    
    static func getUID()->String?{
        let keychain = Keychain(service: "com.apps2you.AlNashra")
        return keychain["uid"]
    }
    
    static func saveUID(uid:String){
        let keychain = Keychain(service: "com.apps2you.AlNashra")
        keychain["uid"] = uid
    }
    
    static func removeUID(){
        let keychain = Keychain(service: "com.apps2you.AlNashra")
        do{
            try keychain.remove("uid")
        }
        catch let ex{
            print(ex)
        }
    }
    
    static func getHeaders()->[String:Any]{
        var params = [String:Any]()
        //params["uid"] = "3123213"
        if let uid = getUID(){
            params["uid"] = uid
        }else{
            let uid = UIDevice.current.identifierForVendor!.uuidString
            saveUID(uid:uid)
            params["uid"] = uid
        }
        
        params["token"] = "test_token"
        params["user-token"] = "test_session_token"
        params["longitude"] = ""
        params["latitude"] = ""
        params["language"] = "ar"
        return params
    }
    
    static func getHomePage(pageNumber:Int,handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/getHomePage"
        var params = [String:Any]()
        params["page_number"] = pageNumber
        
         ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: params,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func getCategoryPage(categoryID:Int,pageNumber:Int,handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/getCategoryPage"
        var params = [String:Any]()
        params["category_id"] = categoryID
        params["page_number"] = pageNumber
        
         ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: params,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func getCategories(handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/getCategories"
        
         ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: nil,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func getGroups(handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/getGroups"
        
         ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: nil,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func likeArticle(articleID:Int,like:Bool,handler:@escaping APICompletionHandler){
        let url = hostURL+"/api/mobile/likeArticle"
        var params = [String:Any]()
        params["article_id"] = articleID
        params["like_status"] = like ? 1 : 0
        
         ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: params,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func followSource(sourceID:Int,follow:Bool,handler:@escaping APICompletionHandler){
        let url = hostURL+"/api/mobile/followSource"
        var params = [String:Any]()
        params["source_id"] = sourceID
        params["follow"] = follow ? 1 : 0
        
         ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: params,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func getSourcesByGroupID(groupID:Int,handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/getSourceByGroupID"
        var params = [String:Any]()
        params["group_id"] = groupID
        
         ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: params,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func getSourceDetails(sourceID:Int,handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/getSourceDetails"
        var params = [String:Any]()
        params["source_id"] = sourceID
        
         ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: params,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func getArticleDetails(articleID:Int,handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/getArticleDetails"
        var params = [String:Any]()
        params["article_id"] = articleID
        
        ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: params,methodPost:.post,logLevel: .verbose) { (success, result) in
             handler(success,result)
         }
    }
    
    static func setSectionNotification(sectionID:Int,enable:Int,handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/setSectionNotification"
        var params = [String:Any]()
        params["section_id"] = sectionID
        params["notification_enable"] = enable
         
        ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: params,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func getStreams(handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/getLiveStreaming"
        
         ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: nil,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func getMySources(handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/getMySources"
        
         ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: nil,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func sendMessage(fName:String,lName:String,email:String,phone:String,message:String,handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/sendMessage"
        var params = [String:Any]()
        params["fname"] = fName
        params["lname"] = lName
        params["email"] = email
        params["mobile"] = phone
        params["message"] = message
         
        ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: params,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func reportFlipped(articleIDs:[Int],handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/reportFlipped"
        var params = [String:Any]()
        params["article_id"] = articleIDs
         
        ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: params, bodyparams: nil,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func reportViewed(articleID:Int,handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/reportViewed"
        var params = [String:Any]()
        params["article_id"] = articleID
         
        ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: nil, bodyparams: params,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
    
    static func setArticleRead(articleIDs:[Int],isRead:Bool,handler:@escaping APICompletionHandler){
        
        if !Helper.isInternetAvailable(){
            handler(false,nil)
            return
        }
        
        let url = hostURL+"/api/mobile/setArticleRead"
        var params = [String:Any]()
        params["article_id"] = articleIDs
        params["is_read"] = isRead ? 1 : 0
        
        ServiceEngine.dispatchRequest(path: url,header:getHeaders(), uriparams: params, bodyparams: params,methodPost:.post) { (success, result) in
             handler(success,result)
         }
    }
}
