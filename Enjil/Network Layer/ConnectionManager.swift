//
//  ReachablityEngine.swift
//  Bareq
//
//  Created by Imac on 11/28/19.
//  Copyright © 2019 Christopher Nassar. All rights reserved.
//

import UIKit
import Reachability

class ConnectionManager {

    static let sharedInstance = ConnectionManager()
    private var reachability : Reachability!

    func observeReachability(){
        self.reachability = try? Reachability()
        NotificationCenter.default.addObserver(self, selector:#selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
        do {
            try self.reachability.startNotifier()
        }
        catch(let error) {
            print("Error occured while starting reachability notifications : \(error.localizedDescription)")
        }
    }

    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .cellular,.wifi:
             print("Network is available.")
             NotificationCenter.default.post(name: NSNotification.Name("CONNECTION_AV"), object: nil)
            break
        case .none,.unavailable:
            print("Network is not available.")
            NotificationCenter.default.post(name: NSNotification.Name("CONNECTION_LOST"), object: nil)
            break
        }
      }
}
