//
//  AppDelegate.swift
//  Enjil
//
//  Created by Imac on 1/15/20.
//  Copyright © 2020 Christopher Nassar. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var restrictRotation = true
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        initializeFirebase()
        initalizeIQKeyboard()
        return true
    }
    
    func initializeFirebase(){
        //Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
    }
    
    func registerDevice(application:UIApplication){
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()

    }
    
    func initalizeIQKeyboard(){
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldPlayInputClicks = true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if restrictRotation{
            return .portrait
        }
        return .all
    }

}

extension AppDelegate:UNUserNotificationCenterDelegate{
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
       InstanceID.instanceID().instanceID { (result, error) in
           if let error = error {
               print("Error fetching remote instange ID: \(error)")
           } else if let result = result {
               print("Remote instance ID token: \(result.token)")
           }
       }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
}

