//
//  ViewController.swift
//  Enjil
//
//  Created by Imac on 1/15/20.
//  Copyright © 2020 Christopher Nassar. All rights reserved.
//

import UIKit
import FolioReaderKit


class ViewController: UIViewController {

    let folioReader = FolioReader()
    var bookPath: String? {
        return Bundle.main.path(forResource: "bible", ofType: "epub")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setCover()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.open()
    }
    
    private func readerConfiguration() -> FolioReaderConfig {
        let config = FolioReaderConfig(withIdentifier: "Bible")
        config.shouldHideNavigationOnTap = false
        config.scrollDirection = .horizontal
        
            // See more at FolioReaderConfig.swift
    //        config.canChangeScrollDirection = false
    //        config.enableTTS = false
    //        config.displayTitle = true
    //        config.allowSharing = false
    //        config.tintColor = UIColor.blueColor()
    //        config.toolBarTintColor = UIColor.redColor()
    //        config.toolBarBackgroundColor = UIColor.purpleColor()
    //        config.menuTextColor = UIColor.brownColor()
    //        config.menuBackgroundColor = UIColor.lightGrayColor()
    //        config.hidePageIndicator = true
    //        config.realmConfiguration = Realm.Configuration(fileURL: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("highlights.realm"))

            // Custom sharing quote background
            config.quoteCustomBackgrounds = []
            if let image = UIImage(named: "demo-bg") {
                let customImageQuote = QuoteImage(withImage: image, alpha: 0.6, backgroundColor: UIColor.black)
                config.quoteCustomBackgrounds.append(customImageQuote)
            }

            let textColor = UIColor(red:0.86, green:0.73, blue:0.70, alpha:1.0)
            let customColor = UIColor(red:0.30, green:0.26, blue:0.20, alpha:1.0)
            let customQuote = QuoteImage(withColor: customColor, alpha: 1.0, textColor: textColor)
            config.quoteCustomBackgrounds.append(customQuote)

            return config
        }
    
    fileprivate func open() {
        let readerConfiguration = self.readerConfiguration()
        folioReader.presentReader(parentViewController: self, withEpubPath: bookPath!, andConfig: readerConfiguration, shouldRemoveEpub: false)
    }

    private func setCover() {

        do {
            let image = try FolioReader.getCoverImage(self.bookPath!)

            //button?.setBackgroundImage(image, for: .normal)
        } catch {
            print(error.localizedDescription)
        }
    }


}

