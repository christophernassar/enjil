//
//  Helper.swift
//  Rawa
//
//  Created by Imac on 5/6/19.
//  Copyright © 2019 Imac. All rights reserved.
//

import UIKit
import SystemConfiguration
import CoreLocation
import CoreTelephony
import AudioToolbox
public typealias ImageCompletionHandler = (_ data:Data) -> Void
public typealias PopupContinueHandler = () -> Void

enum LANGUAGE:String {
    case EN="en"
    case AR="ar"
}

public typealias MessageViewButtonAction = () -> Void
public typealias openUrlCompletionHandler = ()->Void

enum Vibration {
    case error
    case success
    case warning
    case light
    case medium
    case heavy
    case selection
    case oldSchool
    
    func vibrate() {
        
        switch self {
        case .error:
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.error)
            
        case .success:
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
            
        case .warning:
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.warning)
            
        case .light:
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.impactOccurred()
            
        case .medium:
            let generator = UIImpactFeedbackGenerator(style: .medium)
            generator.impactOccurred()
            
        case .heavy:
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
            
        case .selection:
            let generator = UISelectionFeedbackGenerator()
            generator.selectionChanged()
            
        case .oldSchool:
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
        
    }
    
}
class KeyboardService: NSObject {
    static var serviceSingleton = KeyboardService()
    var measuredSize: CGRect = CGRect.zero
    
    @objc class func keyboardHeight() -> CGFloat {
        let keyboardSize = KeyboardService.keyboardSize()
        return keyboardSize.size.height
    }
    
    @objc class func keyboardSize() -> CGRect {
        return serviceSingleton.measuredSize
    }
    
    private func observeKeyboardNotifications() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(self.keyboardChange), name: UIResponder.keyboardDidShowNotification, object: nil)
    }
    
    private func observeKeyboard() {
        let field = UITextField()
        UIApplication.shared.windows.first?.addSubview(field)
        field.becomeFirstResponder()
        field.resignFirstResponder()
        field.removeFromSuperview()
    }
    
    @objc private func keyboardChange(_ notification: Notification) {
        guard measuredSize == CGRect.zero, let info = notification.userInfo,
            let value = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
            else { return }
        
        measuredSize = value.cgRectValue
    }
    
    override init() {
        super.init()
        observeKeyboardNotifications()
        observeKeyboard()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension String
{
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
}

extension UIView {
    
    class func getAllSubviews<T: UIView>(from parenView: UIView) -> [T] {
        return parenView.subviews.flatMap { subView -> [T] in
            var result = getAllSubviews(from: subView) as [T]
            if let view = subView as? T { result.append(view) }
            return result
        }
    }
    
    func getAllSubviews<T: UIView>() -> [T] {
        return UIView.getAllSubviews(from: self) as [T]
    }
}
extension UIView {
    
    var firstViewController: UIViewController? {
        let firstViewController = sequence(first: self, next: { $0.next }).first(where: { $0 is UIViewController })
        return firstViewController as? UIViewController
    }
    
}

enum FontSize:CGFloat{
    case small = 0
    case medium = 0.5
    case big = 1
}

class Helper {
    fileprivate static var currentLanguage = getLanguage()
    fileprivate static var currentFontSize = getFontSize()
    static let animationDuration = 0.2
    static var loadingView:UIView!

    static var regularFont = "Almarai-Regular"
    static var lightFont = "Almarai-Light"
    static var extraBoldFont = "Almarai-ExtraBold"
    static var boldFont = "Almarai-Bold"
    
    static var alertWindow:UIWindow! = nil
    static let windowTag = 1000
    
    static let lightGreenColor = UIColor(red: 63/255, green: 210/255, blue: 47/255, alpha: 1)
    static let greenColor = UIColor(red: 0, green: 123/255, blue: 95/255, alpha: 1)
    static let redColor = UIColor(red: 245/255, green: 40/255, blue: 40/255, alpha: 1)
    static let darkRedColor = UIColor(red: 155/255, green: 0, blue: 0, alpha: 1)
    
    static var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    static func setFontSizeForLabel(label:UILabel,initialFont:CGFloat? = nil){
        if let font = label.font{
            let sizeIncrement = Helper.getFontSizePointIncrement()
            label.font = UIFont(name: font.fontName, size: (initialFont ?? font.pointSize) + sizeIncrement)
        }
    }
    
    static func formatNumber(count: Int)->String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        if let formattedNumber = numberFormatter.string(from: NSNumber(value:count)){
            return formattedNumber
        }
        return ""
    }
    
    static func openLinkInSafari(urlString : String){
        guard let str = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        if let url = URL(string: str){
            UIApplication.shared.open(url)
        }
    }
    
    static func SYSTEM_VERSION_EQUAL_TO(version: String) -> Bool {
        let systemVersionArr = UIDevice.current.systemVersion.split(separator: ".")
        if let first = systemVersionArr.first{
            return first.compare(version, options: .numeric) == .orderedSame
        }
        return false
    }
    
    static func implementGradientLeftRight(view:UIView,leftColor:UIColor,rightColor:UIColor){
        let colorLeft =  leftColor.cgColor
        let colorRight = rightColor.cgColor

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorLeft, colorRight]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0)
        gradientLayer.frame = view.bounds
        
        view.layer.sublayers?.removeAll()
        view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    static func implementGradientBottomTop(view:UIView){
        let colorTop =  UIColor(white: 0, alpha: 0).cgColor
        let colorBottom = UIColor(white: 0, alpha: 0.7).cgColor

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds

        view.layer.sublayers?.removeAll()
        view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    static func implementGradientTopBottom(view:UIView){
        let colorTop =  UIColor(white: 0, alpha: 0.7).cgColor
        let colorBottom = UIColor(white: 0, alpha: 0).cgColor

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds

        view.layer.sublayers?.removeAll()
        view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    static func toggleLanguage(){
        if  Helper.isArabic(){
            let storyboard = UIStoryboard(name: "Main_en", bundle: nil)
            if let vc = storyboard.instantiateInitialViewController(){
                vc.modalTransitionStyle = .crossDissolve
                Helper.setLanguage(lang: .EN)
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
                UIApplication.shared.keyWindow?.rootViewController = vc
            }
        }else{
            let storyboard = UIStoryboard(name: "Main_ar", bundle: nil)
            if let vc = storyboard.instantiateInitialViewController(){
                vc.modalTransitionStyle = .crossDissolve
                Helper.setLanguage(lang: .AR)
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
                UIApplication.shared.keyWindow?.rootViewController = vc
            }
        }
    }
    
    static func resetRoot(){
        let storyboard = getStoryboard()
        if let vc = storyboard.instantiateInitialViewController(){
            vc.modalTransitionStyle = .crossDissolve
            UIApplication.shared.keyWindow?.rootViewController = vc
        }
    }
    
    
    public static func getAppDelegate() -> AppDelegate{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate
    }
    
    public static func getOwnWindowController()->UIViewController? {
        if let _ = alertWindow{
        }else{
            alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.rootViewController?.view.tag = windowTag
            alertWindow.windowLevel = UIWindow.Level.alert + 1;
        }
       
        alertWindow.makeKeyAndVisible()
        if let presentedController = alertWindow.rootViewController?.presentedViewController{
            return presentedController
        }
        
        return alertWindow.rootViewController
    }
    
    public static func restrictRotation(_ restrict:Bool){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.restrictRotation = restrict
    }
    
    public static func DisplayLoading(container:UIViewController){
        
        OperationQueue.main.addOperation { //added
            if(loadingView == nil){
                
                let window = UIApplication.shared.keyWindow
                let top:CGFloat = 0//window?.safeAreaInsets.top ?? 0
                let btm:CGFloat = 0//window?.safeAreaInsets.bottom ?? 0
                
                loadingView = UIView(frame: CGRect(x: 0, y: top, width: container.view.frame.size.width, height: container.view.frame.size.height-btm-top))
                loadingView.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
                loadingView.clipsToBounds = true
                
                let indicator = UIActivityIndicatorView(style: .whiteLarge)
                indicator.color = .white//UIColor.init(red: 72/255, green: 175/255, blue: 230/255, alpha: 1)
                indicator.center = loadingView.center
                indicator.startAnimating()
                loadingView.addSubview(indicator)
            }
            if((loadingView.superview) != nil){
                loadingView.removeFromSuperview()
            }
            
            container.view.addSubview(loadingView)
        }
        
    }
    
    public static func DisplayLoadingView(view:UIView){
                
        let loadingView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        loadingView.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
        loadingView.clipsToBounds = true
        loadingView.tag = 100
        
        let indicator = UIActivityIndicatorView(style: .whiteLarge)
        indicator.color = .white//UIColor.init(red: 72/255, green: 175/255, blue: 230/255, alpha: 1)
        indicator.center = loadingView.center
        indicator.startAnimating()
        loadingView.addSubview(indicator)
        view.addSubview(loadingView)
    }
    
    public static func hideLoading(){
        OperationQueue.main.addOperation {
            if((loadingView != nil) && (loadingView.superview) != nil){
                loadingView.removeFromSuperview()
            }
        }
    }
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
    static func isInternetAvailable() -> Bool{
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    static func isValidPhone(value: String) -> Bool {
        let phoneRegEx = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegEx)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    static func isValidEmail(value:String) -> Bool {
        if value.count == 0{
            return true
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: value)
    }
    
    static func isValidUsername(value:String) -> Bool {
        
        if value.count == 0{
            return true
        }
        
        let usernameRegEx = "[A-Z0-9a-z_]{4,15}"
        let usernameTest = NSPredicate(format:"SELF MATCHES %@", usernameRegEx)
        return usernameTest.evaluate(with: value)
    }
    
    static func isValidDisplayname(value:String?) -> Bool {
        
        if let value = value{
            if value.count == 0{
                return true
            }
            
            if value.count<4{
                return false
            }
            return true
        }
        return false
       
    }
    
    public static func getHostURL()->String{
        if let env = Bundle.main.infoDictionary!["API_URL"] as? String{
            return env
        }
        
        return ""
    }
    
    public static func getResourceURL()->String{
        if let env = Bundle.main.infoDictionary!["RES_URL"] as? String{
            return env
        }
        
        return ""
    }
    
    public static func getPushNotificationURL()->String{
        if let env = Bundle.main.infoDictionary!["PN_URL"] as? String{
            return env
        }
        
        return ""
    }
    
    public static func getPushNotificationGUID()->String{
        if let env = Bundle.main.infoDictionary!["PN_GUID"] as? String{
            return env
        }
        
        return ""
    }
    
    
    static func isArabic()->Bool{
        return (Helper.getLanguage() == .AR)
    }
    
    static func getLanguageID()->Int{
        if let lang = UserDefaults.standard.string(forKey: "bareq_language") {
            switch(lang){
            case "en":
                return 1
            case "ar":
                return 2
            default:
                break
            }
        }
        return 1
    }
    
    static func getDateString(str:String)->String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"

        if let date = dateFormatterGet.date(from: str) {
            return dateFormatterPrint.string(from: date)
        }
        return ""
    }
    
    static func getDate(str:String)->Date?{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"

        if let date = dateFormatterGet.date(from: str) {
            return date
        }
        return nil
    }
    
    static func timeInterval(timeAgo:String) -> String
    {
        let from:String = "منذ"
        if let dateWithTime = Helper.getDate(str: timeAgo){
            let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateWithTime, to: Date())
            if let day = interval.day, day > 0 {
                return day == 1 ? "منذ يوم" : "days"
            }else if let hour = interval.hour, hour > 0 {
                return hour == 1 ? "" + "منذ ساعة" : hour == 2 ? "منذ ساعتان" : "\(from)" + " " + "\(hour)" + " " + "ساعات"
            }else if let minute = interval.minute, minute > 0 {
                return minute == 1 ? "" + "منذ دقيقة" : minute == 2 ? "منذ دقيقتان" : "\(from)" + " " +  "\(minute)" + " " + "دقيقة"
            }else if let second = interval.second, second > 0 {
                return second == 1 ? "" + "منذ ثانية" : "\(from)" + " " +  "\(second)" + " " + "ثوانٍ"
            } else {
                return "منذ لحظات"
            }
        }
        return ""
    }
    
    static func getDateNashra(str:String)->String{
       let dateFormatterGet = DateFormatter()
       dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

       let dateFormatterPrint = DateFormatter()
       dateFormatterPrint.dateFormat = "dd MMM yyyy"
       dateFormatterPrint.locale = Locale(identifier: "ar_AR")
        
       let time_ago = Helper.timeInterval(timeAgo: str)
       if let date = dateFormatterGet.date(from: str) {
            
            if time_ago != "days"{
                return time_ago
            }
            else{
                return dateFormatterPrint.string(from: date)
            }
       }
        
        return ""
    }
    
    static func getLanguage()->LANGUAGE{
        if let lang = UserDefaults.standard.string(forKey: "bareq_language") {
            
            switch(lang){
            case "en":
                return .EN
            case "ar":
                return .AR
            default:
                break
            }
            
        } else {
            // we set a default, just in case
            UserDefaults.standard.set("ar", forKey: "bareq_language")
            UserDefaults.standard.synchronize()
        }
        
        return .AR
    }
    
    static func setLanguage(lang:LANGUAGE){
        currentLanguage = lang
        UserDefaults.standard.set(currentLanguage.rawValue, forKey: "bareq_language")
        UserDefaults.standard.synchronize()
    }
    
    static func setFontSize(fontSize:FontSize){
        currentFontSize = fontSize
        UserDefaults.standard.set(fontSize.rawValue, forKey: "bareq_fontsize")
        UserDefaults.standard.synchronize()
    }
    
    static func getFontSize()->FontSize{
        let size = UserDefaults.standard.float(forKey: "bareq_fontsize")
        if let fontSize = FontSize(rawValue: CGFloat(size)){
            return fontSize
        }
        return .small
    }
    
    static func getFontSizePointIncrement()->CGFloat{
        switch getFontSize() {
        case .small:
            return 0
        case .medium:
            return 5
        case .big:
            return 10
        }
    }
    
    // MARK: Shadow
    static func addShadowToView(view: UIView, bottomShadow: Bool){
        let rectShape = CAShapeLayer()
        rectShape.bounds = view.frame
        rectShape.position = view.center
        if(bottomShadow){
            rectShape.path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.bottomLeft , .bottomRight ], cornerRadii: CGSize(width: 10, height: 10)).cgPath
        }else{ //top shadow
            rectShape.path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.topLeft , .topRight ], cornerRadii: CGSize(width: 10, height: 10)).cgPath
        }
        //Here I'm masking the textView's layer with rectShape layer
        view.layer.mask = rectShape
    }
    
    static func goToAppStore(){
        let itunesLink = "itms-apps://itunes.apple.com/app/id1489959722"
        UIApplication.shared.open(URL(string:itunesLink)!, options: [:], completionHandler: nil)
    }
    
    // MARK: Share
    static func shareLinkWithUrlString(urlString : String , controller : UIViewController){
        guard let str = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return }
        let myWebsite = URL(string: str)
        
        guard let url = myWebsite else {
            print("nothing found")
            return
        }
        
        let shareItems:Array = [url]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.print, .postToWeibo, .addToReadingList, .postToVimeo]
        controller.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK:
    static func getStoryboard() -> UIStoryboard {
        if Helper.isArabic(){
            return UIStoryboard.init(name: "Main_ar", bundle: nil)
        }
        return UIStoryboard.init(name: "Main_en", bundle: nil)
    }
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    var localized: String {
        
        let lang = Helper.currentLanguage
        
        let path = Bundle.main.path(forResource: lang.rawValue, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    mutating func setlocalized(){
        self = localized
    }
    
    func fileName() -> String {
        return URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
    }
    
    func fileExtension() -> String {
        return URL(fileURLWithPath: self).pathExtension
    }
    
    func trim() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func width()->CGFloat{
        return self.frame.size.width
    }
    
    func height()->CGFloat{
        return self.frame.size.height
    }
    
    func x()->CGFloat{
        return self.frame.origin.x
    }
    
    func y()->CGFloat{
        return self.frame.origin.y
    }
    
    func bottom()->CGFloat{
        return self.height()+self.y()
    }
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFill, appCache:Bool = true,handler:ImageCompletionHandler? = nil) {
        
        contentMode = mode
        if url.description.contains("default.jpg"){
            contentMode = .scaleAspectFill
        }
        
        
        if appCache{
            if let data = CachingInterface.getImageData(url: url.description) {
                if let handler = handler{
                    handler(data)
                }else{
                    self.image = UIImage(data: data)
                }
                return
            }
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                    return
            }
            
            CachingInterface.storeImage(data: data, url: url.description)
            if let handler = handler{
                handler(data)
            }else{
                DispatchQueue.main.async() {
                    self.alpha = 0
                    self.image = image
                    
                    UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseInOut, animations: {
                        self.alpha = 1
                    }, completion: nil)
                    }
                }
            }.resume()
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width)  - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height)  - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y);
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension Dictionary {
    var dataRepresentation: Data? {
        return try? JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
    }
    
    var jsonStringRepresentation: String? {
        guard let theJSONData = try? JSONSerialization.data(withJSONObject: self,
                                                            options: []) else {
                                                                return nil
        }
        
        return String(data: theJSONData, encoding: .utf8)
    }
}

public extension UIColor {
    convenience init(_ hex: Int, alpha: Double = 1.0) {
        self.init(red: CGFloat((hex>>16)&0xFF)/255.0, green: CGFloat((hex>>8)&0xFF)/255.0, blue: CGFloat((hex)&0xFF)/255.0, alpha: CGFloat(255 * alpha) / 255)
    }
    
    convenience init(_ hexString: String, alpha: Double = 1.0) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (r, g, b) = ((int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (r, g, b) = (int >> 16, int >> 8 & 0xFF, int & 0xFF)
        default:
            (r, g, b) = (1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(255 * alpha) / 255)
    }
}

extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension String {
    
    func isValidURL() -> Bool {
        guard !contains("..") else { return false }
        
        let head     = "((http|https)://)?([(w|W)]{3}+\\.)?"
        let tail     = "\\.+[A-Za-z]{2,3}+(\\.)?+(/(.)*)?"
        let urlRegEx = head+"+(.)+"+tail
        
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        return urlTest.evaluate(with: trimmingCharacters(in: .whitespaces))
    }
}

extension UILabel{
    func flipToEnglish(){
        self.transform = CGAffineTransform(scaleX: -1, y: 1)
        if self.textAlignment == .left{
            self.textAlignment = .right
        }else if self.textAlignment == .right{
            self.textAlignment = .left
        }
    }
}

extension UITextView{
    func flipToEnglish(){
        self.transform = CGAffineTransform(scaleX: -1, y: 1)
        if self.textAlignment == .left{
            self.textAlignment = .right
        }else if self.textAlignment == .right{
            self.textAlignment = .left
        }
    }
}

extension UIImageView{
    func flipToEnglish(){
        self.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
}

extension UITableViewCell{
    func flipToEnglish(){
        self.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
}

extension UIButton{
    func flipToEnglish(){
        self.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        if self.contentHorizontalAlignment == .left{
            self.contentHorizontalAlignment = .right
        }else if self.contentHorizontalAlignment == .right{
            self.contentHorizontalAlignment = .left
        }
    }
}

extension UIScrollView {
    func scrollToBottom(animated: Bool) {
        if self.contentSize.height < self.bounds.size.height { return }
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: animated)
    }
}

extension String {
    var isArabic: Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", "(?s).*\\p{Arabic}.*")
        return predicate.evaluate(with: self)
    }
    
    mutating func addPrefixIfNeeded(_ prefix: String, requiredPrefix: String? = nil) {
        guard !self.hasPrefix(requiredPrefix?.lowercased() ?? prefix.lowercased()) else { return }
        self = prefix + self
    }
}
extension UIApplication {

    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {

        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)

        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)

        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

extension Date {
    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
}

extension String {

    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
}

extension String {

    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }

    func isValid(regex: RegularExpressions) -> Bool { return isValid(regex: regex.rawValue) }
    func isValid(regex: String) -> Bool { return range(of: regex, options: .regularExpression) != nil }

    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter { CharacterSet.decimalDigits.contains($0) }
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }

    func makeACall() {
        guard   isValid(regex: .phone),
                let url = URL(string: "tel://\(self.onlyDigits())"),
                UIApplication.shared.canOpenURL(url) else { return }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}

class CachingInterface {
    fileprivate static let cacheInstance = UserDefaults.standard
    fileprivate static let firstTimeKey = "FirsTime"
    
    static func isFirstTime()->Bool{
        if let _ = ServiceInterface.getUID(){
            setFirstTime()
            return false
        }
        
        if let _ = cacheInstance.object(forKey: firstTimeKey){
            return false
        }
       
        return true
    }
    
    static func setFirstTime(){
        cacheInstance.set(true, forKey: firstTimeKey)
        cacheInstance.synchronize()
    }
    
    static func storeImage(data:Data, url:String){
        cacheInstance.set(data, forKey: url)
    }
    
    static func getImageData(url:String)->Data?{
        return cacheInstance.data(forKey:url)
    }
    
}
